// Initialisation des variables
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
users = [];
connections = [];
// Dire au serveur d'écouter sur le port 3000
server.listen(process.env.PORT || 3000);
console.log('Serveur démarré...');

// Initialiser la route / pour l'app chat qui prend en réponse le index.html
app.get('/', function (request, response) {
    response.sendFile(__dirname + '/index.html');
});

// Les fonctions permettant de mettre en marche les sockets
io.sockets.on('connection', function (socket) {
    //La connection
    connections.push(socket);
    console.log('Connected: %s sockets connected', connections.length);

    //La déconnection
    socket.on('disconnect', function (data) {
        users.splice(users.indexOf(socket.username), 1);
        updateUsernames();
        connections.splice(connections.indexOf(socket), 1);
        console.log('Disconnect: %s sockets connected', connections.length);
    });

    // Envoyer un message
    socket.on('send message', function (data) {
        //console.log(Object.entries(data));
        //console.log(Object.values(data));
        io.sockets.emit('new message', {msg: data, user: socket.username});
    });

    // La création des users
    socket.on('new user', function (data, callback) {
        callback(true);
        socket.username = data;
        users.push(socket.username);
        updateUsernames();
    });
    // Afficher les noms des utilisateurs présents
    function updateUsernames() {
        io.sockets.emit('get users', users)
    }
});